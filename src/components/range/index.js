import React from 'react';

const Range = ({title, handleCriteria, id}) =>{
    const handleChange = (value, index) => handleCriteria((prev) => {
        const prevObj = {...prev};
        prevObj[id][index] = Number(value)
        return prevObj;
    })

    return  <div>
    <p>{title}</p>
    <label >From</label>
    <input onChange={(e) => handleChange(e.target.value, 0)} />
    <label>To</label>
    <input onChange={(e) => handleChange(e.target.value, 1)} />
</div>
}

export default Range;
