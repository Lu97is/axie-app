import React, { useEffect, useState } from 'react';
import BodyParts from '../../utils/body-parts.json';
import { ItemsSelected, SearchItem, SearchResults, SelectedItem } from './styled';

const Search = ({handleCriteria, id}) =>{
    const [parts, setParts] = useState([]);
    const [selectedParts, setSelectedParts] = useState([])
    const handleSearch = (value) => {
        const matches = BodyParts.filter(({name}) => name.toLowerCase().includes(value.toLowerCase()));
        setParts(value ? matches : []);
    }

    const handleAdd = (name) => {
        if (selectedParts.findIndex((e) => e === name) === -1) {
            setSelectedParts([...selectedParts, name])
        }
    }

    useEffect(() => {
        handleCriteria((prev) => {
            const prevValue = {...prev};
            prevValue[id] = selectedParts;
            return prevValue;
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedParts])

    return  <div>
    <p>Search Parts</p>
    <input onChange={(e) => handleSearch(e.target.value)} />
    <ItemsSelected>
        {(selectedParts).map((name) => <SelectedItem key={name} onClick={() => setSelectedParts(selectedParts.filter((n) => name !== n))}>{name}</SelectedItem>)}
    </ItemsSelected>
    <SearchResults>
    {
        parts.map(({name, partId}) => <SearchItem onClick={() => handleAdd(name)} key={partId}>{name}</SearchItem>)
    }
    </SearchResults>
</div>
};

export default Search;