import styled from 'styled-components';

export const SelectedItem = styled.div`
    background-color: #046cfc;
    color: #ffffff;
    padding: 10px 20px;
    max-width: 100px;
    margin: 12px 12px;
    flex: 0 0 12%;

`;

export const ItemsSelected = styled.div`
    display: flex;
    flex-wrap: wrap;
`;

export const SearchResults = styled.div`
    height: 250px;
    overflow-y: auto;
`;

export const SearchItem = styled.div`
    margin: 12px;
    padding: 24px;
    border: 1px solid #dedede;
`;