import React, { useState } from 'react';
import { Container, Option, SelectItem } from './styled';

const Select = ({items, handleCriteria, id}) => {
    const [selected, setSelected] = useState([]);

    const handleAdd = (name) => {
        let payload = []
        if (selected.findIndex((e) => e === name) === -1) {
            payload = ([...selected, name])
        } else {
            payload = (selected.filter((n) => name !== n))
        }
        handleCriteria((prev) => {
            const newObject = {...prev};
            newObject[id] = payload;
            return newObject;
        })
        setSelected(payload);
    }
    return <Container>
    <p>Title</p>
    <SelectItem>
        {items.map((item) => <Option isActive={selected.includes(item)} onClick={() => handleAdd(item)}>{item}</Option>)}
    </SelectItem>
</Container>
}


export default Select;
