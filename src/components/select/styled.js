import styled from 'styled-components';

export const SelectItem = styled.div`
    width: 100%;
    display: flex;
    flex-wrap: wrap;
`;

export const Container = styled.div`
    padding: 14px;
`;


export const Option = styled.button`
    background-color: ${({isActive}) => isActive ? '#d0d0d0' : '#ffffff'};
    color: ${({isActive}) => isActive ? '#ffffff' : '#000000'};
    outline: none;
    border: 1px solid red;
    padding: 8px 38px;
    margin: 12px 2.5%;
    width: 45%;
`;