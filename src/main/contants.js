export const classes = [
    'Beast', 'Bug', 'Bird', 'Plant', 'Aquatic', 'Reptile'
];

export const stage = ['egg', 'adult'];

export const MIN_VALUE = 27;
export const MAX_VALUE = 61;
export const MAX_BREED = 7;
export const MAX_PURENESS = 6;