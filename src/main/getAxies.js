const { getAxiePureness } = require("../utils/utils");
const { GET_AXIE_LIST, GET_AXIE_BY_ID } = require("./query");

export async function getAxies(criteria, setResults, client, minPureness = 97, size) {
    const {data: { axies: { results } }} = await client.query({
        query: GET_AXIE_LIST,
        variables: {
            auctionType: 'Sale',
            size: 90,
            from: size - 50,
            criteria: {
                classes: ['Aquatic'],
                breedCount: [0,0],
                pureness: [6, 6],
                parts: ["back-goldfish", "ears-nimo", "tail-koi", "tail-shrimp", "horn-shoal-star", "horn-oranda"]
            }
            // criteria
        }
    });
    const payload = [];
    const urls = [];
    for (const { id } of results) {
        const {data: { axie: { genes, id: axieId, image, class: axieClass, auction: { currentPriceUSD }}} } = await client.query({
            query: GET_AXIE_BY_ID,
            variables: {
                axieId: id
            }
        })
        const { quality } = getAxiePureness(genes, axieClass.toLowerCase());
        if(quality >= minPureness) {
            payload.push({ axieId, genes, image, currentPriceUSD })
            urls.push(`https://marketplace.axieinfinity.com/axie/${axieId}`);
        }
    }
    console.log(urls);
    setResults(payload);
}