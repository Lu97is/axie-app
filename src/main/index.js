import React, { useState } from 'react';
import { client } from '..';
import Range from '../components/range';
import Search from '../components/search';
import Select from '../components/select';
import { classes } from './contants';
import { getAxies } from './getAxies';
import { Card, CardInner, CardsContainer, Container, FilterContainer } from './styled';
const Main = () => {
    const [axies, setAxies] = useState([]);
    const [pureness, setPureness] = useState(97);
    const [initialCount, setInitialCount] = useState(50);
    const [criteria, setCriteria] = useState({
        classes: [],
        breedCount: [0,0],
        pureness: [],
        // parts: [],
        parts: ["back-goldfish", "ears-nimo", "tail-koi", "tail-shrimp", "horn-shoal-star", "horn-oranda"],
        hp: [],
        speed: [],
        skill: [],
        morale: [],
        stages: [4]
    });
   return <Container>
       <FilterContainer>
            Filters
            <button onClick={() => getAxies(criteria, setAxies, client, pureness, initialCount)}>Search</button>

            <label>Min Pureness</label>
            <input max={100} type="number" onChange={(e) => setPureness(Number(e.target.value))} />
            <Select handleCriteria={setCriteria} id="classes" items={classes} />
            <Range handleCriteria={setCriteria} id="breedCount" title="Breed Count" />
            <Range handleCriteria={setCriteria} id="pureness" title="Pureness" />
            <Range handleCriteria={setCriteria} id="hp" title="Health" />
            <Range handleCriteria={setCriteria} id="speed" title="Speed" />
            <Range handleCriteria={setCriteria} id="skill" title="Skill" />
            <Range handleCriteria={setCriteria} id="morale" title="Morale" />
            <Search handleCriteria={setCriteria} id="parts" />
       </FilterContainer>
       <CardsContainer>
       {axies.map(({axieId, genes, image, currentPriceUSD}) => <Card key={axieId}>
        <CardInner>
        <i>${currentPriceUSD}</i>
        <img style={{width: '50px', height: '50px'}} src={image} alt="axie" />
        <a target="_blank" rel="noreferrer" href={`https://marketplace.axieinfinity.com/axie/${axieId}`}>Buy</a>
        </CardInner>
        </Card>)}
        <button onClick={() => {
            setInitialCount(initialCount + 50);
            getAxies(criteria, setAxies, client, pureness, initialCount);
        }} >Search More</button>
   </CardsContainer>
   </Container>
}

export default Main;

