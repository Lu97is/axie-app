const { gql } = require("@apollo/client");

export const GET_AXIE_LIST = gql`
    query GetAxieBriefList($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String){
        axies(
            auctionType: $auctionType,
            criteria: $criteria,
            from: $from,
            sort: $sort,
            size: $size,
            owner: $owner
        ){
            total
            results{
                id
                parts{
                    id
                    specialGenes
                }
            }
        }
    }
`;

export const GET_AXIE_BY_ID = gql`
    query GetAxieDetail($axieId: ID!) {
        axie(axieId:$axieId) {
            id
            genes
            class
            image
            auction{
                currentPriceUSD
            }
        }
    }
`;


// Partes dominante agua
// Partes r1 todos agua
// Partes r2 por lo menos 3 de agua