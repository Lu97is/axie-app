import styled from 'styled-components';

export const Container = styled.div`
    max-width: 1450px;
    margin: 20px auto;
    display: flex;
    padding: 40px;
`;

export const FilterContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 40%;
`;

export const CardsContainer = styled.div`
    display: flex;
    flex-direction: column;
    /* flex-wrap: wrap; */
    max-height: 820px;
    overflow-y: auto;
    width: 60%;
`;

export const Card = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding:13px;
`;

export const CardInner = styled.div`
    padding: 15px;
    border: 1px solid red;
`;